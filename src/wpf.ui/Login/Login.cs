﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace wpf.ui
{
    public class LoginDialog
    {
        public static Task<LoginDialogData> GetAutenticationDataTask(string initialUserName = "")
        {
            var mainview = Application.Current.MainWindow as MetroWindow;
            var loginTask = mainview.ShowLoginAsync(
                "Auth",
                "Provide name and password",
                new LoginDialogSettings
                {
                    AnimateShow = true,
                    AnimateHide = true,
                    InitialUsername = initialUserName,
                    AffirmativeButtonText = "Enter",
                    NegativeButtonText = "Close",
                    NegativeButtonVisibility = Visibility.Visible,
                    UsernameWatermark = "Name",
                    PasswordWatermark = "Password",
                    EnablePasswordPreview = true
                });

            return loginTask;
        }

    }
}
