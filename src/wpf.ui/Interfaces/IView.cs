﻿namespace wpf.ui
{
    public interface IView
    {
        IViewModel ViewModel { get; set; }

        void Configure(UiShowOptions options);
    }
}