﻿namespace wpf.ui
{
    public interface IDockWindow
    {
        string Title { get; set; }
    }
}