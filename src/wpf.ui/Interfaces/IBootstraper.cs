﻿namespace wpf.ui
{
    public interface IBootstraper
    {
        IShell Init();
    }
}
