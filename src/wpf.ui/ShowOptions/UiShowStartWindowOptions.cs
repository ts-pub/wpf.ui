﻿using AvalonDock;

namespace wpf.ui
{
    public class UiShowStartWindowOptions : UiShowOptions
    {
        public DockingManager DockingManager { get; set; }
    }
}