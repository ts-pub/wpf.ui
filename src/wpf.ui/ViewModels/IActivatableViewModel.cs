﻿namespace wpf.ui
{
    public interface IActivatableViewModel
    {
        void Activate(ViewRequest viewRequest);
    }
}
