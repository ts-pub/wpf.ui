﻿namespace wpf.ui
{
    public interface IInitializableViewModel
    {
        void Initialize(ViewRequest viewRequest);
    }
}
