﻿namespace wpf.ui
{
    public class ViewModelCloseQueryArgs
    {
        public bool IsCanceled { get; set; }
    }
}