﻿namespace wpf.ui
{
    public interface IResultableViewModel<TResult>
    {
        TResult ViewResult { get; set; }
    }
}