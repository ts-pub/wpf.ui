﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Reactive.Disposables;
using System.ComponentModel;
using FluentValidation;
using System.Linq;
using DynamicData;

namespace wpf.ui
{
    public abstract class ViewModelBase : ReactiveObject, IViewModel
    {
        [Reactive]
        public string Title { get; set; }

        [Reactive]
        public string FullTitle { get; set; }

        [Reactive]
        public bool IsBusy { get; set; }

        [Reactive]
        public bool IsEnabled { get; set; } = true;

        [Reactive]
        public bool CanHide { get; set; } = true;

        [Reactive]
        public bool CanClose { get; set; } = true;

        public string ViewId { get; internal set; }

        #region CloseQuery


        public void Close()
        {
            var args = new ViewModelCloseQueryArgs();

            CloseQuery?.Invoke(this, args);

            if (args.IsCanceled)
                return;
        }


        protected internal virtual void Closed(ViewModelCloseQueryArgs args)
        {
            DisposeInternals();
        }

        protected internal virtual void Closing(ViewModelCloseQueryArgs args)
        {

        }

        protected virtual void DisposeInternals()
        {
            Disposables.Clear();
            CloseQuery = null;
        }

        internal event EventHandler<ViewModelCloseQueryArgs> CloseQuery;

        #endregion

        protected internal CompositeDisposable Disposables { get; }  = new CompositeDisposable();
    }//end of class
}
