﻿using ReactiveUI;

namespace wpf.ui
{
    public class ColumnDescriptor : ReactiveObject
    {
        public string HeaderText { get; set; }
        public string DisplayMemberMember { get; set; }
        public bool IsDatetime { get; set; }
        public string DatetimeFormat { get; set; }
    }
}
