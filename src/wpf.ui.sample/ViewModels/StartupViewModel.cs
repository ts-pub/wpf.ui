﻿using System;
using System.Collections.Generic;
using System.Text;

namespace wpf.ui.sample.ViewModels
{
    public class StartupViewModel : ViewModelBase, IInitializableViewModel
    {
        private readonly IShell shell;

        public const string StartupViewId = "StartupView";

        public StartupViewModel(IShell s)
        {
            shell = s;
        }

        public void Initialize(ViewRequest viewRequest)
        {
        }
    }//end of vm
}
