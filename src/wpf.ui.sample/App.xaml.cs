﻿using System.Windows;
using wpf.ui.sample.ViewModels;
using wpf.ui.sample.Views;

namespace wpf.ui.sample
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var shell = UiStarter.Start<IDockWindow>(
                new Bootstrapper(),
                new UiShowStartWindowOptions
                {
                    Title = "Orville",
                    DockingManager = new VsDockingManager()
                });

            shell.ShowView<StartupView>(
                viewRequest: new ViewRequest { ViewId = StartupViewModel.StartupViewId },
                options: new UiShowOptions { Title = "Start Page", CanClose = false });

            /*shell.ShowToolIn<OutputView>(VsDockingManager.ToolsBottom,
                viewRequest: new ViewRequest { ViewId = OutputViewModel.OutputViewId },
                options: new UiShowOptions { Title = "Output", CanClose = false });*/
        }
    }
}
